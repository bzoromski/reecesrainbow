jQuery(document).ready(function($){

    if ( $('.childgrant-container .image-gallery').length ) {
        $('.image-gallery').lightSlider({ 
            gallery:true, 
            item:1, 
            auto:false, 
            loop:true, 
            thumbItem: 9,
            adaptiveHeight: true,
        });
    };
  
  });