<?php

/**
 * Plugin Name: Reece's Rainbow
 * Author: Brian Zoromski
 * Description: Grants plugin for Reece's Rainbow
 * Version: 1.6.2
 */

include_once('classes/ChildGrants.php');
include_once('classes/Reports.php');
//include_once('classes/FamilyGrants.php');

class ReecesRainbowGrants {
    public function custom_log($message) { 
        if(is_array($message)) { 
            $message = json_encode($message); 
        } 
        else {
            $message = str_replace('<br>',"\n",$message);
        }
        $uploads  = wp_upload_dir( null, false );
        $logs_dir = $uploads['basedir'] . '/rr-logs';

        if ( ! is_dir( $logs_dir ) ) {
            mkdir( $logs_dir, 0755, true );
        }

        $file = fopen( $logs_dir . '/' . 'log.log', 'a' );
        $date = new DateTime('NOW', new DateTimeZone('UTC'));
		$dateInTimezone = $date->setTimezone(new DateTimeZone(wp_timezone_string()));
		$str = "[" . $dateInTimezone->format("Y/m/d g:i:s A") . "] " . $message;
        fwrite($file, $str . "\n" ); 
        fclose($file); 
    }

    public function setUp() {
        $childgrants = new ChildGrants();
        // load ACF fields
        $childgrants->update_fields_from_json();
        //$familygrants = new FamilyGrants();
        $reports = new Reports();
    }

}

$reecesrainbowGrants = new ReecesRainbowGrants();
$reecesrainbowGrants->setUp();