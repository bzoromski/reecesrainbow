<?php

class ChildGrants {

    public function __construct(){
        $this->db = $GLOBALS['wpdb'];
    
        /***** REGISTER CUSTOM POST TYPE *****/
        add_action('init', [$this, 'register_childgrants_cpt']);

        /***** Register taxonomy for special grants *****/
        add_action('init', [$this, 'register_special_grants_tag']);

        /***** Register taxonomy for countries *****/
        add_action('init', [$this, 'register_country_tag']);

        /***** Register taxonomy for diagnoses *****/
        add_action('init', [$this, 'register_diagnosis_tag']);

        /***** Register taxonomy for regions *****/
        add_action('init', [$this, 'register_region_tag']);

        /***** Register taxonomy for availability categories *****/
        add_action('init', [$this, 'register_availability_tag']);

        /***** Register taxonomy for sizeable grant categories *****/
        add_action('init', [$this, 'register_sizeable_grants_tag']);

        /***** ADD A SHORTCODE *****/
        add_shortcode('childgrants', [$this, 'display_childgrants']);

        /***** Add import to menu *****/
        add_action( 'admin_menu', [$this, 'register_childgrants_import_page']);

        /***** Add options to menu *****/
        add_action( 'admin_menu', [$this, 'register_childgrants_options_page']);

        /***** Add shortcode help to menu *****/
        add_action( 'admin_menu', [$this, 'register_childgrants_ref_page']);

        /* load custom content  */
        add_filter( 'the_content', [$this, 'filter_childgrant_content'] );

         /* hide specific posts from archive pages */
        add_action( 'pre_get_posts', [$this, 'hide_specific_posts_from_archives'], 1 );

        /* Set up WP cron jobs */
        add_action('init', [$this, 'setup_cron']);

        /* Register deactivation hook to unschedule cron jobs */
        register_deactivation_hook( __FILE__, [$this, 'deactivation_tasks'] ); 
 
        add_action( 'admin_post_print.csv',  [$this, 'print_csv'] );

        /* enqueue admin CSS */
        add_action( 'admin_enqueue_scripts', [$this, 'enqueue_admin_scripts'] );

        /* load custom goal info  */
        add_filter( 'acf/load_field/name=custom_goal', [$this, 'filter_custom_goal'] );

    }

    //register taxonomy for special grants
    function register_special_grants_tag() {
        $labels = array(
            'name'                       => _x( 'Special Grants', 'taxonomy general name', 'textdomain' ),
            'singular_name'              => _x( 'Special Grant', 'taxonomy singular name', 'textdomain' ),
            'search_items'               => __( 'Search Special Grants', 'textdomain' ),
            'popular_items'              => __( 'Popular Special Grants', 'textdomain' ),
            'all_items'                  => __( 'All Special Grants', 'textdomain' ),
            'parent_item'                => __( 'Parent Special Grant', 'textdomain' ),
            'parent_item_colon'          => __( 'Parent Special Grant:', 'textdomain' ),
            'edit_item'                  => __( 'Edit Special Grant', 'textdomain' ),
            'update_item'                => __( 'Update Special Grant', 'textdomain' ),
            'add_new_item'               => __( 'Add New Special Grant', 'textdomain' ),
            'new_item_name'              => __( 'New Special Grant', 'textdomain' ),
            'separate_items_with_commas' => __( 'Separate special grant names with commas', 'textdomain' ),
            'add_or_remove_items'        => __( 'Add or remove special grants', 'textdomain' ),
            'choose_from_most_used'      => __( 'Choose from the most used special grants', 'textdomain' ),
            'not_found'                  => __( 'No special grants found.', 'textdomain' ),
            'menu_name'                  => __( 'Special Grants', 'textdomain' ),
        );
        register_taxonomy( 
        'special-grants', //taxonomy 
        'childgrant', //post-type
        array( 
            'hierarchical'  => true, 
            'labels'        => $labels,
            'show_ui'       => true,
            'show_admin_column' => true, 
            'rewrite'       => true, 
            'query_var'     => true 
        ));
    }

    // enqueue custom CSS and JS
    function enqueue(): void {
            wp_enqueue_style( 'childgrantcss', plugins_url('reecesrainbow') . '/css/childgrant.css' , array(), '1.0.9' );
            wp_enqueue_style( 'lightslidercss', plugins_url('reecesrainbow') . '/css/lightslider.min.css' , array(), '1.0.0' );
            wp_enqueue_script( 'lightsliderjs', plugins_url('reecesrainbow')  . '/js/lightslider.min.js', array( 'jquery' ), '1.0.0' );
            wp_enqueue_script( 'lightsliderinit', plugins_url('reecesrainbow')  . '/js/lightslider-init.js', array( 'lightsliderjs' ), '1.0.1' );
    }

    // enqueue custom CSS for admin
    function enqueue_admin_scripts(): void {
        
        $currentScreen = get_current_screen();
        // var_dump($currentScreen);
        if ( isset( $currentScreen->base ) && 'childgrant_page_child-grants-options' === $currentScreen->base ) {
            wp_enqueue_style( 'grantoptionscss', plugins_url('reecesrainbow') . '/css/grantoptions.css' , array(), '1.0.0' );
        }
    }

    function filter_custom_goal($field) {
        $custom_goal_name = get_option( 'custom_goal_name' );
        $field['instructions'] = __($custom_goal_name, 'txtdomain');
        return $field;
    }

    //register taxonomy for country codes
    function register_country_tag() {
        $labels = array(
            'name'                       => _x( 'Countries', 'taxonomy general name', 'textdomain' ),
            'singular_name'              => _x( 'Country', 'taxonomy singular name', 'textdomain' ),
            'search_items'               => __( 'Search Countries', 'textdomain' ),
            'all_items'                  => __( 'All Countries', 'textdomain' ),
            'parent_item'                => __( 'Parent Country', 'textdomain' ),
            'parent_item_colon'          => __( 'Parent Country:', 'textdomain' ),
            'edit_item'                  => __( 'Edit Country', 'textdomain' ),
            'update_item'                => __( 'Update Country', 'textdomain' ),
            'add_new_item'               => __( 'Add New Country', 'textdomain' ),
            'new_item_name'              => __( 'New Country', 'textdomain' ),
            'separate_items_with_commas' => __( 'Separate country names with commas', 'textdomain' ),
            'add_or_remove_items'        => __( 'Add or remove countries', 'textdomain' ),
            'choose_from_most_used'      => __( 'Choose from the most used countries', 'textdomain' ),
            'not_found'                  => __( 'No countries found.', 'textdomain' ),
            'menu_name'                  => __( 'Countries', 'textdomain' ),
        );
        register_taxonomy( 
        'country', //taxonomy 
        'childgrant', //post-type
        array( 
            'hierarchical'  => true, 
            'labels'        => $labels,
            'show_ui'       => true,
            'show_admin_column' => true, 
            'rewrite'       => true, 
            'query_var'     => true 
        ));
    }

    //register taxonomy for diagnoses
    function register_diagnosis_tag() {
        $labels = array(
            'name'                       => _x( 'Diagnoses', 'taxonomy general name', 'textdomain' ),
            'singular_name'              => _x( 'Diagnosis', 'taxonomy singular name', 'textdomain' ),
            'search_items'               => __( 'Search Diagnoses', 'textdomain' ),
            'all_items'                  => __( 'All Diagnoses', 'textdomain' ),
            'parent_item'                => __( 'Parent Diagnosis', 'textdomain' ),
            'parent_item_colon'          => __( 'Parent Diagnosis:', 'textdomain' ),
            'edit_item'                  => __( 'Edit Diagnosis', 'textdomain' ),
            'update_item'                => __( 'Update Diagnosis', 'textdomain' ),
            'add_new_item'               => __( 'Add New Diagnosis', 'textdomain' ),
            'new_item_name'              => __( 'New Diagnosis', 'textdomain' ),
            'separate_items_with_commas' => __( 'Separate diagnoses names with commas', 'textdomain' ),
            'add_or_remove_items'        => __( 'Add or remove diagnoses', 'textdomain' ),
            'choose_from_most_used'      => __( 'Choose from the most used diagnoses', 'textdomain' ),
            'not_found'                  => __( 'No diagnoses found.', 'textdomain' ),
            'menu_name'                  => __( 'Diagnoses', 'textdomain' ),
        );
        register_taxonomy( 
        'diagnosis', //taxonomy 
        'childgrant', //post-type
        array( 
            'hierarchical'  => true, 
            'labels'        => $labels,
            'show_ui'       => true,
            'show_admin_column' => true, 
            'rewrite'       => true, 
            'query_var'     => true 
        ));
    }

    //register taxonomy for regions
    function register_region_tag() {
        $labels = array(
            'name'                       => _x( 'Regions', 'taxonomy general name', 'textdomain' ),
            'singular_name'              => _x( 'Region', 'taxonomy singular name', 'textdomain' ),
            'search_items'               => __( 'Search Regions', 'textdomain' ),
            'all_items'                  => __( 'All Regions', 'textdomain' ),
            'parent_item'                => __( 'Parent Region', 'textdomain' ),
            'parent_item_colon'          => __( 'Parent Region:', 'textdomain' ),
            'edit_item'                  => __( 'Edit Region', 'textdomain' ),
            'update_item'                => __( 'Update Region', 'textdomain' ),
            'add_new_item'               => __( 'Add New Region', 'textdomain' ),
            'new_item_name'              => __( 'New Region', 'textdomain' ),
            'separate_items_with_commas' => __( 'Separate region names with commas', 'textdomain' ),
            'add_or_remove_items'        => __( 'Add or remove regions', 'textdomain' ),
            'choose_from_most_used'      => __( 'Choose from the most used regions', 'textdomain' ),
            'not_found'                  => __( 'No regions found.', 'textdomain' ),
            'menu_name'                  => __( 'Regions', 'textdomain' ),
        );
        register_taxonomy( 
        'region', //taxonomy 
        'childgrant', //post-type
        array( 
            'hierarchical'  => true, 
            'labels'        => $labels,
            'show_ui'       => true,
            'show_admin_column' => true, 
            'rewrite'       => true, 
            'query_var'     => true 
        ));
    }

        //register taxonomy for availability
        function register_availability_tag() {
            $labels = array(
                'name'                       => _x( 'Availability Categories', 'taxonomy general name', 'textdomain' ),
                'singular_name'              => _x( 'Availability', 'taxonomy singular name', 'textdomain' ),
                'search_items'               => __( 'Search Availability Categories', 'textdomain' ),
                'all_items'                  => __( 'All Availability Categories', 'textdomain' ),
                'parent_item'                => __( 'Parent Availability', 'textdomain' ),
                'parent_item_colon'          => __( 'Parent Availability:', 'textdomain' ),
                'edit_item'                  => __( 'Edit Availability', 'textdomain' ),
                'update_item'                => __( 'Update Availability', 'textdomain' ),
                'add_new_item'               => __( 'Add New Availability', 'textdomain' ),
                'new_item_name'              => __( 'New Availability', 'textdomain' ),
                'separate_items_with_commas' => __( 'Separate availability names with commas', 'textdomain' ),
                'add_or_remove_items'        => __( 'Add or remove availability categories', 'textdomain' ),
                'choose_from_most_used'      => __( 'Choose from the most used availability categories', 'textdomain' ),
                'not_found'                  => __( 'No availability categories found.', 'textdomain' ),
                'menu_name'                  => __( 'Availability Categories', 'textdomain' ),
            );
            register_taxonomy( 
            'availability', //taxonomy 
            'childgrant', //post-type
            array( 
                'hierarchical'  => true, 
                'labels'        => $labels,
                'show_ui'       => true,
                'show_admin_column' => true, 
                'rewrite'       => true, 
                'query_var'     => true 
            ));
        }
    
        //register taxonomy for sizeable grants
        function register_sizeable_grants_tag() {
            $labels = array(
                'name'                       => _x( 'Sizeable Grants', 'taxonomy general name', 'textdomain' ),
                'singular_name'              => _x( 'Sizeable Grant', 'taxonomy singular name', 'textdomain' ),
                'search_items'               => __( 'Search Sizeable Grants', 'textdomain' ),
                'popular_items'              => __( 'Popular Sizeable Grants', 'textdomain' ),
                'all_items'                  => __( 'All Sizeable Grants', 'textdomain' ),
                'parent_item'                => __( 'Parent Sizeable Grant', 'textdomain' ),
                'parent_item_colon'          => __( 'Parent Sizeable Grant:', 'textdomain' ),
                'edit_item'                  => __( 'Edit Sizeable Grant', 'textdomain' ),
                'update_item'                => __( 'Update Sizeable Grant', 'textdomain' ),
                'add_new_item'               => __( 'Add New Sizeable Grant', 'textdomain' ),
                'new_item_name'              => __( 'New Sizeable Grant', 'textdomain' ),
                'separate_items_with_commas' => __( 'Separate sizeable grant names with commas', 'textdomain' ),
                'add_or_remove_items'        => __( 'Add or remove sizeable grants', 'textdomain' ),
                'choose_from_most_used'      => __( 'Choose from the most used sizeable grants', 'textdomain' ),
                'not_found'                  => __( 'No sizeable grants found.', 'textdomain' ),
                'menu_name'                  => __( 'Sizeable Grants', 'textdomain' ),
            );
            register_taxonomy( 
            'sizeable-grants', //taxonomy 
            'childgrant', //post-type
            array( 
                'hierarchical'  => true, 
                'labels'        => $labels,
                'show_ui'       => true,
                'show_admin_column' => true, 
                'rewrite'       => true, 
                'query_var'     => true 
            ));
        }
    
    /*  Add an Import submenu page under the Child Grants custom post type menu */
    function register_childgrants_import_page() {
        add_submenu_page(
            'edit.php?post_type=childgrant',
            __( 'Import Child Grants', 'text_domain' ),
            __( 'Import Child Grants', 'text_domain' ),
            'publish_posts',
            'child-grants-import',
            array($this, 'childgrants_import_page_content')
        );
    }    

    function register_childgrants_options_page() {
        add_submenu_page(
            'edit.php?post_type=childgrant',
            __( 'Options', 'text_domain' ),
            __( 'Options', 'text_domain' ),
            'publish_posts',
            'child-grants-options',
            array($this, 'childgrants_options_page_content')
        );
    }   

    /* Display content for the childgrants import page */
    function childgrants_import_page_content() { 
        if (isset($_POST['step'])) {
            $step = intval($_POST["step"]);
            }
        else {
            $step = 0;
        }
        $error = false;
        switch ($step) {
            case 0:
                break;
            case 1:
                if (strpos(strtolower($_POST['url']), 'https://docs.google.com/spreadsheets/d/') === false) {
                    echo "<font color='red'>ERROR:</font> Your import URL needs to be a Google Sheets doc.<P>";
                    $error = true;
                    break;
                }
                $error = $this->import_childgrants();
                break;
        }
        echo '<div class="float"> <form enctype="multipart/form-data" action="" method="POST">
			<input type="hidden" name="step" value="1" />
			<B>Google Sheet to Import:</B> <input type="url" name="url" id="url"
                value="https://docs.google.com/spreadsheets/d/1upjDJq7EreeeWgXrh4z05GCKbIu_8p9zx3D8-C-6wts/edit#gid=0"
                pattern="https://.*" size="100"
                required>
                <P>
                <i>Please note that if you change the above from the default Google Sheet, in your new Sheet you will have to go into File/Share and change it to "Anyone on the internet with this link can view"</i>
                <p>
			<input type="submit" value="Import Child Grants" />
			</form> </div>';
        ?>
        
        <?php
    }

    function import_childgrants() { 
        $error = false;
        $str = $_POST['url'];
        $substring_start = strpos($str, '/d/') + 3;
        $size = strpos($str, '/edit', $substring_start) - $substring_start;
        $google_sheets_id = substr($str, $substring_start, $size);
        if ( empty($google_sheets_id) ) {
            $error = true;
        }
        else {
            $google_sheet_csv = 'https://docs.google.com/spreadsheets/d/' . $google_sheets_id . '/export?format=csv&id=' . $google_sheets_id;
            $row = 0;
            $count = 0;
            $imported = 0;
            // Google Sheet/CSV field order:
            $fields = array(
                'child_name' => 0,
                'cause_code' => 1,
                'gender' => 2,
                'birth_date' => 3,
                'sibling_group' => 4,
                'diagnosis' => 5,
                'diagnosis_text' => 6,
                'country_code' => 7,
                'region' => 8,
                'listed_date' => 9,
                'long_description' => 10,
                'available_to_single_mothers' => 11,
                'available_to_single_fathers' => 12,
                'available_to_large_families' => 13,
                'available_to_older_parents' => 14,
                'mffm' => 15,
                'newly_listed' => 16,
                'at_risk_of_aging_out' => 17,
                'matching_grant' => 18,
                'custom_goal' => 19,
                'aged_out' => 20,
                'macc' => 21,
                'sponsor' => 22,
                'private' => 23,
                'ee_grant' => 24,
                'osn_grant' => 25
            );
            if (($handle = fopen($google_sheet_csv, "r")) !== FALSE) 
            {
                $percent = 0;
                session_start();

                ini_set('max_execution_time', 0); // to get unlimited php script execution time

                if(empty($_SESSION['i'])){
                    $_SESSION['i'] = 0;
                }
                ob_start();
                $lines = file($google_sheet_csv, FILE_SKIP_EMPTY_LINES);
                $total = count($lines);
                echo '<div class="percent-complete">% Complete</div>';
                ob_flush();
                flush();
                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) 
                {
                    $row++;
                    $last_percent = $percent;
                    $percent = intval($row/$total * 100);
                    $_SESSION['i'] = $row;
                    if ($percent <> $last_percent) {
                        echo '<style>.percent-complete::before { content: "' . $percent . '"}</style>';
                        ob_flush();
                        flush();
                    }
                    if ( $row > 1 && !empty($data[$fields['child_name']]) )
                    {
                        // print_r($data);
                        $post_array = array(
                            'post_title'    => wp_strip_all_tags( $data[$fields['child_name']] ),
                            'post_content'  => $data[$fields['long_description']],
                            // set author ID to 29 (Michelle) by default
                            'post_author'   => 29,
                            'post_status'   => 'publish',
                            'post_type'     => 'childgrant'
                        );
                        // print_r($post_array);
                        $post_id = wp_insert_post( $post_array, true );
                        if ( is_wp_error($post_id) ) {
                            echo $post_id->get_error_message();
                        }
                        // set diagnosis and country categories
                        $diagnosis = $data[$fields['diagnosis']];
                        // print_r('Diag: ' . $diagnosis);
                        if ( !empty($diagnosis) ) {
                            $args = array(
                                'taxonomy'      => 'diagnosis', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'name'    => $diagnosis
                            );                             
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'diagnosis', true );   
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                     
                                }
                            }
                        }
                        $country = $data[$fields['country_code']];
                        if ( !empty($country) ) {
                            $args = array(
                                'taxonomy'      => 'country', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'name'    => $country
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'country', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $region = $data[$fields['region']];
                        if ( !empty($region) ) {
                            $args = array(
                                'taxonomy'      => 'region', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'name'    => $region
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'region', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $ee_grant = $data[$fields['ee_grant']];
                        if ( $ee_grant === 'TRUE' ) {
                            $args = array(
                                'taxonomy'      => 'special-grants', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'slug'    => 'ee'
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'special-grants', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $osn_grant = $data[$fields['osn_grant']];
                        if ( $osn_grant === 'TRUE' ) {
                            $args = array(
                                'taxonomy'      => 'special-grants', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'slug'    => 'osn-older-grant'
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'special-grants', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $single_mothers = $data[$fields['available_to_single_mothers']];
                        if ( $single_mothers === 'TRUE' ) {
                            $args = array(
                                'taxonomy'      => 'availability', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'slug'    => 'single-mothers'
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $single_fathers = $data[$fields['available_to_single_fathers']];
                        if ( $single_fathers === 'TRUE' ) {
                            $args = array(
                                'taxonomy'      => 'availability', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'slug'    => 'single-fathers'
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $large_families = $data[$fields['available_to_large_families']];
                        if ( $large_families === 'TRUE' ) {
                            $args = array(
                                'taxonomy'      => 'availability', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'slug'    => 'large-families'
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        $older_parents = $data[$fields['available_to_older_parents']];
                        if ( $older_parents === 'TRUE' ) {
                            $args = array(
                                'taxonomy'      => 'availability', 
                                'hide_empty'    => false,
                                'fields'        => 'all',
                                'slug'    => 'older-parents'
                            ); 
                            $terms = get_terms( $args );                            
                            $count = count($terms);
                            if($count > 0){
                                foreach ($terms as $term) {
                                    $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                                    if( is_wp_error( $return ) ) {
                                        echo $return->get_error_message();
                                    }                        
                                }
                            }
                        }
                        foreach($fields as $field_key => $sheet_column) {
                            // skip fields already handled as post content or category)
                            $columns_to_skip = array($fields['long_description'], $fields['country_code'], $fields['region'], $fields['diagnosis'], 
                                $fields['ee_grant'], $fields['osn_grant'], $fields['available_to_single_mothers'], $fields['available_to_single_fathers'],
                                $fields['available_to_large_families'], $fields['available_to_older_parents']);
                            if ( !in_array($sheet_column, $columns_to_skip) ) {
                                if ( $this->is_boolean_field($field_key) ) {
                                    if ( $data[$sheet_column] === 'TRUE' ) {
                                        $value = 1;
                                    }
                                    else {
                                        $value = 0;
                                    }
                                }
                                else {
                                    $value = $data[$sheet_column];
                                }
                                update_field( $field_key, $value, $post_id );
                            }
                        }
                        $imported++;                
                    }
                    $count++;
                }
                fclose($handle);
                echo '<style>.percent-complete::before { content: "100"}</style>';
                echo "Imported " . $imported . " child grant";
                if ( $imported > 1 ) {
                    echo "s";
                } 
                echo "<P>";
                ob_flush();
                flush();
                ob_end_clean();  
                session_destroy(); 
            }
            else {
                $error = true;
                echo '<font color="red">ERROR:</font> Your import URL could not be opened as a CSV - please make sure the sharing settings are set to "Anyone on the internet with this link can view".<P>';
            }
        }
        return $error;
    }

    /*  Add a submenu page under the Child Grants custom post type menu */
    function register_childgrants_ref_page() {
        add_submenu_page(
            'edit.php?post_type=childgrant',
            __( 'Child Grants Shortcode Reference', 'text_domain' ),
            __( 'Shortcode Reference', 'text_domain' ),
            'publish_posts',
            'child-grants-ref',
            array($this, 'childgrants_ref_page_content')
        );
    }    
    
    /* Display content for the childgrants shortcode documentation */
    function childgrants_ref_page_content() { ?>
        <style>
            ul { 
                list-style: disc;
            }
            li {
                margin-left: 1.25em;
            }
        </style>
        <div class="wrap">
        <h2>Child Grant Shortcode Reference</h2>
        Shortcode to use:<P>
        <code>[childgrants]</code>
        <P>Options:
        <ul>
            <li><code>gender</code> - boy or girl; e.g., <code>[childgrants gender="boy"]</code>
            <li><code>age</code> - "0-2", "0-5", "3-5", "6-9", "10+"; e.g., <code>[childgrants age="0-2"]</code>
            <li><code>diagnosis</code> - slug of the <a href="/wp-admin/edit-tags.php?taxonomy=diagnosis&post_type=childgrant">diagnosis category</a>; e.g., <code>[childgrants diagnosis="down-syndrome"]</code>
            <li><code>diagnosis_is_not</code> - slug of a  <a href="/wp-admin/edit-tags.php?taxonomy=diagnosis&post_type=childgrant">diagnosis category</a> that should be excluded; e.g., for Other Special Needs, <code>[childgrants diagnosis_is_not="down-syndrome"]</code>
            <li><code>mffm</code> - true; e.g., <code>[childgrants mffm=true]</code>
            <li><code>newly_listed</code> - true
            <li><code>sibling_group</code> - true
            <li><code>at_risk_of_aging_out</code> - true
            <li><code>matching_grant</code> - true
            <li><code>custom_goal</code> - true
            <li><code>aged_out</code> - true
            <li><code>macc</code> - true
            <li><code>sponsor</code> (I Have a Warrior) - true
        </ul>
        All options can be combined as well, for example: <code>[childgrants gender="boy" diagnosis="down-syndrome" age="0-2"]</code>
        </div>
        <?php
    }


    /* Display content for the childgrants options page */
    function childgrants_options_page_content() { 
        if (isset($_POST['step'])) {
            $step = intval($_POST["step"]);
            }
        else {
            $step = 0;
        }
        $error = false;
        switch ($step) {
            case 0:
                break;
            case 1:
                $error = $this->update_childgrants($manual = true);
                break;
            case 2:
                $error = update_option( 'angel_tree_category', $_POST['cat'] );
                break;
            case 4:
                $error = update_option( 'custom_goal_name', $_POST['custom_goal_name'] );
                $error = update_option( 'custom_goal_amount', $_POST['custom_goal_amount'] );
                break;
            case 5:
                $error = update_option( 'sponsor_page_id', $_POST['page_id'] );
                $error = update_option( 'sponsor_image_url', $_POST['sponsor_image_url'] );
                $error = update_option( 'sponsor_image_url_height', $_POST['sponsor_image_url_height'] );
                $error = update_option( 'sponsor_image_url_width', $_POST['sponsor_image_url_width'] );
                break;
            case 6:
                $error = $this->remove_sponsors();
                break;
        }
        $custom_goal_name = get_option( 'custom_goal_name' );
        $custom_goal_amount = get_option( 'custom_goal_amount' );
        $sponsor_page_id = get_option( 'sponsor_page_id' );
        $sponsor_image_url = get_option( 'sponsor_image_url' );
        $sponsor_image_url_height = get_option( 'sponsor_image_url_height' );
        $sponsor_image_url_width = get_option( 'sponsor_image_url_width' );

        echo '<p><div class="wrap"> <form action="" method="POST">
			<input type="hidden" name="step" value="1" />
			<input type="submit" name="submit" class="page-title-action" value="Update Grant Categories" /> - This manually assigns special grants based on age (e.g., Ages 6-9 Older Child Grant (DS)), sizeable grants (e.g., Grants $2,500+) and sets Newly Listed, ';
        echo $custom_goal_name;
        echo ' & At Risk of Aging Out
            </form> </div>';
        echo '<p><div class="wrap"> <form action="" method="POST">
			<input type="hidden" name="step" value="2" />
			<input type="submit" name="submit" class="page-title-action" value="Set Current MACC Availability Category" /> ';
        wp_dropdown_categories( 'taxonomy=availability&order=DESC' ); 
        echo '- Assigns category to be current MACC category
			</form> </div>';
        echo '<p><div class="wrap"> <form action="" method="POST">
			<input type="hidden" name="step" value="3" />';
        echo '<a class="page-title-action" value="Export MACC Grant Listing" href="' . admin_url( 'admin-post.php?action=print.csv&list=macc-goals' ) . '">Export MACC Grant Listing</a>
		     - This exports grants and their donation IDs for current MACC category
			</form> </div>';
        echo '<p><div class="wrap option-group"> <form action="" method="POST">
			<div class="custom-goal-name"><input type="hidden" name="step" value="4" />
            <span class="field-name">Custom Goal Name:</span> <input type="text" name="custom_goal_name" value="';
        echo $custom_goal_name;
		echo '"></div><p><div class="option-group-amount"><span class="field-name">Custom Goal Amount:</span> <span class="option-group-amount-entry"><input type="number" name="custom_goal_amount" value="';
        echo $custom_goal_amount;
        echo '"></span></div><p><input type="submit" name="submit" class="page-title-action" value="Update Custom Goal Info" /> 
            <p><span class="field-name">Note:</span> If you change the custom goal amount and want it to take effect immediately, please run Update Grant Categories.
			</form> </div>';
        echo '<p><div class="wrap option-group"> <form action="" method="POST">
			<div class="sponsor-page-id"><input type="hidden" name="step" value="5" />
            <span class="field-name">MACC Warrior Page:</span> ';
        wp_dropdown_pages(array ('selected' => $sponsor_page_id ));
		echo '</div><p><div class="sponsor-image-url"><span class="field-name">MACC Warrior Image URL:</span> <input type="text" name="sponsor_image_url" value="';
        echo $sponsor_image_url;
        echo '"></div><p><span class="option-group-amount"><span class="field-name">Image Height:</span> <span class="option-group-amount-entry"><input type="number" name="sponsor_image_url_height" value="';
        echo $sponsor_image_url_height;
        echo '"></span><span class="option-group-amount"><span class="field-name">Image Width:</span> <span class="option-group-amount-entry"><input type="number" name="sponsor_image_url_width" value="';
        echo $sponsor_image_url_width;
        echo '"></span></span><p><input type="submit" name="submit" class="page-title-action" value="Update MACC Warrior Info" /> 
			</form> </div>';
        echo '<p><div class="wrap"> <form action="" method="POST">
			<input type="hidden" name="step" value="6" />
			<input type="submit" name="submit" class="page-title-action" value="Remove Warriors from All Grants" /> - This unchecks the "I Have a Warrior" checkbox for all child grants
            </form> </div>';
    }


    function print_csv() {
        if ( ! current_user_can( 'manage_options' ) )
            return;

        $list = htmlspecialchars($_GET['list']);
        $filename = $list . ".csv";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Pragma: no-cache');

        if ($list === 'macc-goals') {
            $array_data = $this->export_macc_grant_listing();
        }
        $out = fopen('php://output', 'w');
        foreach($array_data as $row){
          fputcsv($out, $row);
        }
        fclose($out);
    }

    function export_macc_grant_listing() {
        $term_id = get_option( 'angel_tree_category' );
        $args = array(
            'posts_per_page' => '-1',
            'post_status'    => 'publish',
            'orderby'        => 'title',
            'order'          => 'ASC',
            'post_type'      => 'childgrant',
            'tax_query' => array(
            array(
                'taxonomy' => 'availability',
                'field'    => 'term_id',
                'terms'    => $term_id,
            ),
            )
        );
        
        $out = array();
        
        $titles = array('Goal ID', 'Goal Name', 'URL');
        array_push($out, $titles);
        $query = new WP_Query( $args);

        if ( $query->have_posts() ):
            while ( $query->have_posts() ): $query->the_post();
                $row = array(
                    get_field( "cause_code" ),
                    get_the_title(),
                    get_permalink(),
                );
                array_push($out, $row);
            endwhile;
        endif;
        wp_reset_postdata();
        return $out;     
    }

    function convert_availabilty_acfs() {
        $args = array(
            'post_type' => 'childgrant',
            'orderby' => 'modified',
            'order'   => 'DESC',
            'nopaging' => true,
        );
        // echo "<br>";
        $childgrant_query = new WP_Query($args);
        // var_dump($childgrant_query);
        $count = 0;
        $mother_availability_grant_count = 0;
        $father_availability_grant_count = 0;
        $older_availability_grant_count = 0;
        $large_availability_grant_count = 0;
        
        if ( $childgrant_query->have_posts() ) {
            // echo "made it to IF<br>";
            while ( $childgrant_query->have_posts() ) {
                $count++;
                $childgrant_query->the_post();
                // echo "In loop, checking post # " . $count . " for " . get_the_title() . "<br>";
                $post_id = get_the_ID();
                $available_to_single_mothers = get_field('available_to_single_mothers', $post_id );
                if ( !empty($available_to_single_mothers) ) {
                    $args = array(
                        'taxonomy'      => 'availability', 
                        'hide_empty'    => false,
                        'fields'        => 'all',
                        'slug'    => 'single-mothers'
                    ); 
                    $terms = get_terms( $args );                            
                    $count = count($terms);
                    if($count > 0){
                        foreach ($terms as $term) {
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                            if( is_wp_error( $return ) ) {
                                echo $return->get_error_message();
                            }                        
                        }
                        $mother_availability_grant_count++;
                    }
                }
                $available_to_single_fathers = get_field('available_to_single_fathers', $post_id );
                if ( !empty($available_to_single_fathers) ) {
                    $args = array(
                        'taxonomy'      => 'availability', 
                        'hide_empty'    => false,
                        'fields'        => 'all',
                        'slug'    => 'single-fathers'
                    ); 
                    $terms = get_terms( $args );                            
                    $count = count($terms);
                    if($count > 0){
                        foreach ($terms as $term) {
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                            if( is_wp_error( $return ) ) {
                                echo $return->get_error_message();
                            }                            
                        }
                        $father_availability_grant_count++;
                    }
                }
                $available_to_older_parents = get_field('available_to_older_parents', $post_id );
                if ( !empty($available_to_older_parents) ) {
                    $args = array(
                        'taxonomy'      => 'availability', 
                        'hide_empty'    => false,
                        'fields'        => 'all',
                        'slug'    => 'older-parents'
                    ); 
                    $terms = get_terms( $args );                            
                    $count = count($terms);
                    if($count > 0){
                        foreach ($terms as $term) {
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                            if( is_wp_error( $return ) ) {
                                echo $return->get_error_message();
                            }                        
                        }
                        $older_availability_grant_count++;
                    }
                }
                $available_to_large_families = get_field('available_to_large_families', $post_id );
                if ( !empty($available_to_large_families) ) {
                    $args = array(
                        'taxonomy'      => 'availability', 
                        'hide_empty'    => false,
                        'fields'        => 'all',
                        'slug'    => 'large-families'
                    ); 
                    $terms = get_terms( $args );                            
                    $count = count($terms);
                    if($count > 0){
                        foreach ($terms as $term) {
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'availability', true );
                            if( is_wp_error( $return ) ) {
                                echo $return->get_error_message();
                            }                        
                        }
                        $large_availability_grant_count++;
                    }
                }
            } // end child grant query loop
            $output .= $mother_availability_grant_count . " Available to Single Mother fields converted to availability category<br>";
            $output .= $father_availability_grant_count . " Available to Single Father fields converted to availability category<br>";
            $output .= $older_availability_grant_count . " Available to Older Parents fields converted to availability category<br>";
            $output .= $large_availability_grant_count . " Available to Large Families fields converted to availability category<br>";
            echo $output;
            // Restore original Post Data
            wp_reset_postdata();
        } // end if have_posts
    }

    function remove_sponsors() {
        $args = array(
            'post_type' => 'childgrant',
            'post_status' => 'publish',
            'meta_key'      => 'sponsor',
            'meta_value'    => '1',
            'nopaging' => true,
        );
        // echo "<br>";
        $childgrant_query = new WP_Query($args);
        // var_dump($childgrant_query);
        $count = 0;
        if ( $childgrant_query->have_posts() ) {
            // echo "made it to IF<br>";
            while ( $childgrant_query->have_posts() ) {
                $count++;
                // echo "Count:" . strval($count) . '<br>';
                $childgrant_query->the_post();
                update_field('sponsor', 0);
            } // end child grant query loop
            $output = strval($count) . " child grant" . ($count > 1 ? "s" : "") . ' updated to no longer be set to "I Have a Warrior"<br>';
            // 
            echo $output;
            // Restore original Post Data
            wp_reset_postdata();
        } // end if have_posts
        else {
            echo 'No child grants found set to "I Have a Warrior"<br>';
        }
    
    }
    
    public function filter_childgrant_content( $content ) {
        global $post;
        global $childgrant_listing;

        // Check if we're inside the main loop in a single post page.
        if (in_the_loop() && is_main_query() && $post->post_type == 'childgrant' ) {
            $this->enqueue();
            $childgrant_before_content = '';
            $childgrant_after_content = '';
            if (get_field('mffm') == 1) {
                $childgrant_before_content .= '<div class="mffm-details"><a href="/youcanhelp/mffm">My Family Found Me</a></div>';
            }

            $childgrant_before_content .= '<div class="childgrant-container">';

            $images = get_field('photos'); 
            if ( ( $images ) && !is_null( $images ) ){
                $childgrant_before_content .= '<ul id="light-slider" class="image-gallery">';
                foreach( $images as $image ) {
                    $childgrant_before_content .= '<li data-thumb="' . $image['url'] . '">';
                    $childgrant_before_content .= '<img src="' . $image['url'] . '" alt="<' . $image['alt'] . '" /></li>';        
                } 
                $childgrant_before_content .= '</ul>';
            }
            else if ( !is_null ( $images ) ) {
                // for some reason get_field is returning an array of False for gallery ACF fields when not in the shortcode
                // this code displays the first photo by itself in that instance, without a gallery
                $images_list = get_field('photos',$post->ID,false); 
                $first_image = $images_list[0];
                if ( !empty( $first_image ) ) {
                    $childgrant_before_content .= '<div class="single-image">';
                    ob_start();
                    echo wp_get_attachment_image( $images_list[0], "thumbnnail", "", array( "class" => "img-responsive" ) ); 
                    $childgrant_before_content .= ob_get_clean();
                    $childgrant_before_content .= '</div>';
                }
            }

            $childgrant_before_content .= '<div class="child-details">';

            if ( !empty(get_field('sponsor') )) {
                $sponsor_page_id = get_option( 'sponsor_page_id' );
                $sponsor_image_url = get_option( 'sponsor_image_url' );
                $sponsor_image_url_height = get_option( 'sponsor_image_url_height' );
                $sponsor_image_url_width = get_option( 'sponsor_image_url_width' );
                $childgrant_before_content .= '<div class="sponsor-image-container"><a href="' . get_permalink($sponsor_page_id) . '"><img class="size-full sponsor-image" src="' . $sponsor_image_url . '" alt="I Have a MACC Warrior" height="' . $sponsor_image_url_height .'" width="' . $sponsor_image_url_width . '" /></a></div>';
            }    
        

            if (get_field('sibling_group') == 1) {
                $sibling_group = true;
            }
            else {
                $sibling_group = false;
            }

            if ($sibling_group) {
                $childgrant_before_content .= '<div class="sibling-birth-details"><div class="sibling-header">Sibling Group</div>';
                $childgrant_before_content .= '<div class="sibling-age-details">';
            }
            else {
                $childgrant_before_content .=  '<div class="gender-birth-details"><span class="gender-details">' . get_field('gender') . '</span>, ' ;
                $childgrant_before_content .=  '<span class="age-details">';
            }

            // var_dump($post->ID);
            $birth_date = new DateTime((get_field('birth_date')));
            $today   = new DateTime('today', new DateTimeZone(wp_timezone_string()));
            $age = $birth_date->diff($today)->y;
            if ($sibling_group) {
                $childgrant_before_content .=   'Ages: ';
                if ($age == 0) {
                    $childgrant_before_content .=  'Less than one year';
                }
                else {
                    $childgrant_before_content .=  $age;
                }
                for ($i = 1; $i <= 5; $i++) {
                    $birth_date_sibling = get_field('birth_date_sibling_' .$i);
                    if ( !empty($birth_date_sibling) ) {
                        $birth_date_sibling_datetime = new DateTime($birth_date_sibling, new DateTimeZone(wp_timezone_string()));
                        $age_sibling = $birth_date_sibling_datetime->diff($today)->y;
                        if ($age_sibling == 0) {
                            $childgrant_before_content .=  ', less than one year';
                        }
                        else {
                            $childgrant_before_content .=  ', ' . $age_sibling;
                        }
                    }
                } 
                $childgrant_before_content .= '</div>';
            }
            else {
                if ($age == 0) {
                    $childgrant_before_content .=  'born ' . $birth_date->format('Y');
                }
                else {
                    $childgrant_before_content .=   'Age: ' . $age . ' ' .  '</span>';
                }
            }
            $childgrant_before_content .= '</div>';
            
            $countries = get_the_terms( $post->ID, 'country' );
            if (is_array($countries) && !empty($countries)) {
                $childgrant_before_content .= '<div class="country-details">';
                $childgrant_before_content .= '<span class="country-label">Country Code: </span>';
                $childgrant_before_content .= '<span class="country-code">';
                // init counter
                $i = 1;
                foreach ( $countries as $country ) {
                    $country_link = get_term_link( $country );
                    if (! is_wp_error( $country_link ) )
                        $childgrant_before_content .= '<a href="' . $country_link .  '">' . $country->name . '</a>';
                    //  Add comma (except after the last country)
                    $childgrant_before_content .=  ($i < count($countries))? ", " : "";
                    // Increment counter
                    $i++;
                }    
                $childgrant_before_content .= '</span>';
                $childgrant_before_content .= '</div>';
            }

            $regions = get_the_terms( $post->ID, 'region' );
            if (is_array($regions) && !empty($regions)) {
                $childgrant_before_content .= '<div class="region-details">';
                $childgrant_before_content .= '<span class="region-label">Region: </span>';
                $childgrant_before_content .= '<span class="region-name">';
                // init counter
                $i = 1;
                foreach ( $regions as $region ) {
                    $region_link = get_term_link( $region );
                    if (! is_wp_error( $region_link ) )
                        $childgrant_before_content .= '<a href="' . $region_link .  '">' . $region->name . '</a>';
                    //  Add comma (except after the last region)
                    $childgrant_before_content .=  ($i < count($regions))? ", " : "";
                    // Increment counter
                    $i++;
                }    
                $childgrant_before_content .= '</span>';
                $childgrant_before_content .= '</div>';
            }

            $diagnoses_obj_list = get_the_terms( $post->ID, 'diagnosis' );
            if (is_array($diagnoses_obj_list) && !empty($diagnoses_obj_list)) {
                $childgrant_before_content .= '<div class="primary-diagnosis-details">';
                $childgrant_before_content .= '<span class="primary-diagnosis-label">Primary Diagnosis: </span>';
                $childgrant_before_content .= '<span class="primary-diagnosis">';
                // init counter
                $i = 1;
                foreach ( $diagnoses_obj_list as $diagnosis ) {
                    $diagnosis_link = get_term_link( $diagnosis );
                    if (! is_wp_error( $diagnosis_link ) )
                        $childgrant_before_content .= '<a href="' . $diagnosis_link .  '">' . $diagnosis->name . '</a>';
                    //  Add comma (except after the last diagnosis)
                    $childgrant_before_content .=  ($i < count($diagnoses_obj_list))? ", " : "";
                    // Increment counter
                    $i++;
                }                
                $childgrant_before_content .= '</span>';
                $childgrant_before_content .= '</div>';
            }
            if ( !empty(get_field('diagnosis_text')) ) {
                    $childgrant_before_content .= '<div class="diagnosis-details">' . get_field('diagnosis_text') . '</div>';
                }
            if ( !empty(get_field('listed_date') )) {
                $listed_date = get_field('listed_date');
                $childgrant_before_content .= '<div class="listed-date-details"><span class="listed-label">Listed: </span> <span class="listed-date">' . date("M Y", strtotime($listed_date)) . '</span></div>';
            }
            $terms = get_the_terms( $post->ID, 'special-grants' );
            if ( ! empty( $terms ) ) {
                foreach ( $terms as $term ) {
                    $childgrant_before_content .= '<div class="special-grant ' . $term->slug . '">' . $term->description . '</div>';
                }
            }
            if ( !empty(get_field('cause_code')) ) {
                ob_start();
                echo do_shortcode( '[donation-can goal_id=' . get_field('cause_code') . ' show_title=false]' );
                $childgrant_before_content .= ob_get_clean();
            }
            $childgrant_before_content .= '<div class="availability-details">';
            $availability_obj_list = get_the_terms( $post->ID, 'availability' );
            // init counter
            $i = 1;
            foreach ( $availability_obj_list as $availability ) {
                $availability_link = get_term_link( $availability );
                if (! is_wp_error( $availability_link ) )
                    $childgrant_before_content .= '<a href="' . $availability_link .  '">' . $availability->name . '</a>';
                //  Add comma (except after the last availability)
                $childgrant_before_content .=  ($i < count($availability_obj_list))? ", " : "";
                // Increment counter
                $i++;
            }                
            $childgrant_before_content .= '</div>'; /* closes .availability-details div */

            $sizeable_grant_obj_list = get_the_terms( $post->ID, 'sizeable-grants' );
            if ( !empty($sizeable_grant_obj_list) ) {
                // init counter
                $childgrant_before_content .= '<div class="sizeable-grant-details">';
                $i = 1;
                foreach ( $sizeable_grant_obj_list as $sizeable_grant ) {
                    $sizeable_grant_link = get_term_link( $sizeable_grant );
                    if (! is_wp_error( $sizeable_grant_link ) )
                        $childgrant_before_content .= '<a href="' . $sizeable_grant_link .  '">' . $sizeable_grant->name . '</a>';
                    //  Add comma (except after the last sizeable_grant)
                    $childgrant_before_content .=  ($i < count($sizeable_grant_obj_list))? ", " : "";
                    // Increment counter
                    $i++;
                }                
                $childgrant_before_content .= '</div>'; /* closes .sizeable-grant-details div */
            }

            $childgrant_before_content .= '<div class="childgrant-content">';
            $childgrant_after_content .= '</div>'; /* closes .childgrant-content div */

            $childgrant_after_content .= '</div>'; /* closes .child-details div */

            $childgrant_after_content .= '</div>'; /* closes .childgrant-container div */

            return $childgrant_before_content . $content  . $childgrant_after_content;
        }
        return $content;
    }
    
    // load ACF fields from JSON
    public function update_fields_from_json(){

        add_filter('acf/settings/load_json', 'my_acf_json_load_point');

        function my_acf_json_load_point( $paths ) {
            
            // remove original path (optional)
            unset($paths[0]);
            
            // append path
            $paths[] = dirname(__FILE__)  . '/fields';
            
            // return
            return $paths;
            
        }
    }

    function is_boolean_field ($key) {
        $boolean_fields = array('available_to_single_mothers','available_to_single_fathers',
            'available_to_large_families','available_to_older_parents','mffm','newly_listed','sibling_group',
            'at_risk_of_aging_out','matching_grant','custom_goal','aged_out','macc','sponsor','private');
        return in_array($key, $boolean_fields);
    }

    // Register Custom Post Type
    function register_childgrants_cpt() {

        $labels = array(
            'name'                  => _x( 'Child Grants', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Child Grant', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Child Grants', 'text_domain' ),
            'name_admin_bar'        => __( 'Child Grant', 'text_domain' ),
            'archives'              => __( 'Child Grant Archives', 'text_domain' ),
            'attributes'            => __( 'Child Grant Attributes', 'text_domain' ),
            'parent_item_colon'     => __( 'Parent Grant', 'text_domain' ),
            'all_items'             => __( 'All Child Grants', 'text_domain' ),
            'add_new_item'          => __( 'Add New Child Grant', 'text_domain' ),
            'add_new'               => __( 'Add New', 'text_domain' ),
            'new_item'              => __( 'New Child Grant', 'text_domain' ),
            'edit_item'             => __( 'Edit Child Grant', 'text_domain' ),
            'update_item'           => __( 'Update Child Grant', 'text_domain' ),
            'view_item'             => __( 'View Child Grant', 'text_domain' ),
            'view_items'            => __( 'View Child Grants', 'text_domain' ),
            'search_items'          => __( 'Search Child Grants', 'text_domain' ),
            'not_found'             => __( 'No child grants found', 'text_domain' ),
            'not_found_in_trash'    => __( 'No child grants found in Trash', 'text_domain' ),
            'featured_image'        => __( 'Featured Image', 'text_domain' ),
            'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
            'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
            'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
            'insert_into_item'      => __( 'Insert into child grant', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Uploaded to this child grant', 'text_domain' ),
            'items_list'            => __( 'Child grants list', 'text_domain' ),
            'items_list_navigation' => __( 'Child grants list navigation', 'text_domain' ),
            'filter_items_list'     => __( 'Filter child grants list', 'text_domain' ),
        );

        $args = array(
            'label'                 => __( 'Child Grant', 'text_domain' ),
            'description'           => __( 'Grant toward the adoption of one or more children', 'text_domain' ),
            'labels'                => $labels,
            'rewrite'               => array('slug' => 'childgrant'),
            'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
            'taxonomies'            => array( '' ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-universal-access',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
        );
        register_post_type( 'childgrant', $args );

    }

    function hide_specific_posts_from_archives ( $query ) {
        if ( ! is_admin() && is_archive() && is_tax() )  {
            //
            //Get original meta query
            $meta_query = ( is_array( $query->get('meta_query') ) ) ? $query->get('meta_query') : [];
            // hide MFFM grants from archive pages
            $meta_query []	= array(
                'key'	 	=> 'mffm',
                'value'	  	=> true,
                'compare' 	=> '!=',
            );
            // hide aged out grants from archive pages
            $meta_query []	= array(
                'key'	 	=> 'aged_out',
                'value'	  	=> true,
                'compare' 	=> '!=',
            );
            $query->set('meta_query',$meta_query);
            $query->set( 'posts_per_page', 50 );
            return;
        }
    }

    public function display_childgrants($attr) {
        global $paged;
        $posts_per_page = 50;
        $atts = shortcode_atts(array(
            'gender' => '',
            'diagnosis' => '',
            'diagnosis_is_not' => '',
            'country_code' => '',
            'available_to_single_mothers' => '',
            'available_to_single_fathers' => '',
            'available_to_large_families' => '',
            'available_to_older_parents' => '',
            'mffm' => 'false',
            'newly_listed' => '',
            'sibling_group' => '',
            'at_risk_of_aging_out' => '',
            'matching_grant' => '',
            'custom_goal' => '',
            'age' => '',
            'aged_out' => 'false'
          ), $attr); 
        $args = array(
            'post_type' => 'childgrant',
            'orderby' => 'modified',
            'order'   => 'DESC',
            'showposts' => $posts_per_page,
            'paged' => $paged
        );
        // always hide listings set to private
        $meta_query = array('relation' => 'AND');
        $meta_query []	= array(
            'key'	 	=> 'private',
            'value'	  	=> true,
            'compare' 	=> '!=',
        );

        foreach($atts as $key => $value) {  
            if ( !empty($value) ) {
                if ($key == 'diagnosis') {
                    $tax_query = array(
                        array(
                            'taxonomy' => 'diagnosis',
                            'field'    => 'slug',
                            'terms'    => $value,
                        ));
                    $args['tax_query'] = $tax_query;
                }
                else if ($key == 'diagnosis_is_not') {
                    $tax_query = array(
                        array(
                            'taxonomy' => 'diagnosis',
                            'field'    => 'slug',
                            'operator' => 'NOT IN',
                            'terms'    => $value,
                        ));
                    $args['tax_query'] = $tax_query;
                }
                else if ( $this->is_boolean_field($key) ) {
                    $boolean_array = array(
                        'key' => $key,
                        'compare' => '==',
                    );
                    if ( $value == 'true' ) {
                        $boolean_array['value'] = 1;
                    }
                    else {
                        $boolean_array['value'] = 0;
                    }
                    $meta_query [] = $boolean_array;
                }
                else if ($key == 'age') {
                    if ($value == '0-2') {
                        $minus_3_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_3_years_date->sub(new DateInterval('P3Y'))->add(new DateInterval('P1D'));
                        $date_start = $minus_3_years_date->format('Y-m-d');
                        $meta_query [] = array(
                            'relation' => 'OR', // either the main birth date or any sibling birth dates must match
                            array(
                                'key'	 	=> 'birth_date',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_1',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_2',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_3',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_4',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_5',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                        );
                    } elseif ($value == '0-5') {
                        $minus_6_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_6_years_date->sub(new DateInterval('P6Y'))->add(new DateInterval('P1D'));
                        $date_start = $minus_6_years_date->format('Y-m-d');
                        $meta_query [] = array(
                            'relation' => 'OR', // either the main birth date or any sibling birth dates must match
                            array(
                                'key'	 	=> 'birth_date',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_1',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_2',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_3',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_4',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_5',
                                'value' => $date_start,
                                'compare' => '>=', 
                                'type' => 'DATE' 
                            )
                        );
                    } elseif ($value == '3-5') {
                        $minus_6_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_6_years_date->sub(new DateInterval('P6Y'))->add(new DateInterval('P1D'));
                        $date_start = $minus_6_years_date->format('Y-m-d');
                        $minus_3_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_3_years_date->sub(new DateInterval('P3Y'));
                        $date_end = $minus_3_years_date->format('Y-m-d');
                        $meta_query [] = array(
                            'relation' => 'OR', // either the main birth date or any sibling birth dates must match
                            array(
                                'key'	 	=> 'birth_date',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_1',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN',  
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_2',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN',  
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_3',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN',  
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_4',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_5',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN', 
                                'type' => 'DATE' 
                            )
                        );
                    }
                    elseif ($value == '6-9') {
                        $minus_10_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_10_years_date->sub(new DateInterval('P10Y'))->add(new DateInterval('P1D'));
                        $date_start = $minus_10_years_date->format('Y-m-d');
                        $minus_6_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_6_years_date->sub(new DateInterval('P6Y'));
                        $date_end = $minus_6_years_date->format('Y-m-d');
                        $meta_query [] = array(
                            'relation' => 'OR', // either the main birth date or any sibling birth dates must match
                            array(
                                'key'	 	=> 'birth_date',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_1',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN',  
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_2',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN',  
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_3',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN',  
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_4',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_5',
                                'value' => array($date_start, $date_end),
                                'compare' => 'BETWEEN', 
                                'type' => 'DATE' 
                            )
                        );
                    }
                    elseif ($value == '10+') {
                        $minus_10_years_date = new DateTime('today', new DateTimeZone(wp_timezone_string()));
                        $minus_10_years_date->sub(new DateInterval('P10Y'));
                        $date_start = $minus_10_years_date->format('Y-m-d');
                        $meta_query [] = array(
                            'relation' => 'OR', // either the main birth date or any sibling birth dates must match
                            array(
                                'key'	 	=> 'birth_date',
                                'value' => $date_start,
                                'compare' => '<=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_1',
                                'value' => $date_start,
                                'compare' => '<=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_2',
                                'value' => $date_start,
                                'compare' => '<=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_3',
                                'value' => $date_start,
                                'compare' => '<=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_4',
                                'value' => $date_start,
                                'compare' => '<=', 
                                'type' => 'DATE' 
                            ),
                            array(
                                'key'	 	=> 'birth_date_sibling_5',
                                'value' => $date_start,
                                'compare' => '<=', 
                                'type' => 'DATE' 
                            )
                        );
                    }
                }
                else {
                    $meta_query []	= array(
                        'key'	 	=> $key,
                        'value'	  	=> $value,
                        'compare' 	=> '=',
                    );
                }   
            }
        } 
        // var_dump($meta_query);
        $args['meta_query'] = $meta_query;
        $childgrant_query = new WP_Query($args);
        $total_found_posts = $childgrant_query->found_posts;
        $total_page = ceil($total_found_posts / $posts_per_page);
        //var_dump($childgrant_query);
        if ( $childgrant_query->have_posts() ) {
            ob_start();
            echo '<div class="childgrant-listpage">';
            global $childgrant_listing;
            $childgrant_listing = true;
            while ( $childgrant_query->have_posts() ) {
                $childgrant_query->the_post();
                echo '<h2 class="post-title entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
                the_content();
            }
            echo '</div>'; // end childgrant-listpage div
            echo '<div class="childgrant-nav">';
            $big = 999999999; // need an unlikely integer
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $childgrant_query->max_num_pages
            ) );
            // echo '<div class="nav-previous alignleft">' . get_next_posts_link( '&#171; Additional Children', $total_page ); 
            // echo '</div>';
            // echo '<div class="nav-next alignright">' . get_previous_posts_link( 'Additional Children &#187;' ); 
            // echo '</div>';
            echo '</div>'; // end childgrant-nav div
            $childgrant_listing = false;
            $output = ob_get_clean();
            return $output;
            // Restore original Post Data
            wp_reset_postdata();
        }
        else {
            _e('Sorry, no matching child grants were found.'); 
        }
    }
    
    public function daily_updates() {
        ReecesRainbowGrants::custom_log("Starting update_childgrants");
        $this->update_childgrants($manual = false);
        ReecesRainbowGrants::custom_log("Completed update_childgrants");
    }

    function update_childgrants($manual) {
        $error = $this->update_specialgrants($manual);
        if (!$error) {
            $error = $this->update_sizeablegrants($manual);
        }   
        if (!$error) {
            $error = $this->update_all_child_grants($manual);
        }   
        return $error;
    }
    

    function update_specialgrants($manual) {
        $args = array(
            'post_type' => 'childgrant',
            'orderby' => 'modified',
            'order'   => 'DESC',
            'nopaging' => true,
        );
        $tax_query = array(
            array(
                'taxonomy' => 'diagnosis',
                'field'    => 'slug',
                'terms'    => 'down-syndrome',
            ));
        $args['tax_query'] = $tax_query;
        $childgrant_query = new WP_Query($args);
        // var_dump($childgrant_query);
        $today   = new DateTime('today', new DateTimeZone(wp_timezone_string()));
        $ds_6_to_9_count = 0;
        $ds_10_plus_count = 0;
        $ds_6_to_9_removed_from_10_plus = 0;
        
        if ( $childgrant_query->have_posts() ) {
            while ( $childgrant_query->have_posts() ) {
                // echo "<br>";
                $childgrant_query->the_post();
                $birth_date = new DateTime((get_field('birth_date')));
                $age = $birth_date->diff($today)->y;
                $post_id = get_the_ID();
                
                if ( $age >= 6 && $age <= 9 ) {
                    $ds_6_to_9_count++;
                    // echo ' post_id: ' . $post_id;
                    $args = array(
                        'taxonomy'      => 'special-grants', 
                        'hide_empty'    => false,
                        'fields'        => 'all',
                        'slug'    => 'ages-6-9-ds'
                    );                             
                    $terms = get_terms( $args );                            
                    $count = count($terms);
                    if($count > 0){
                        foreach ($terms as $term) {
                            // echo ' term_id: ' . $term->term_id;
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'special-grants', true );   
                            if( is_wp_error( $return ) ) {
                                if ($manual) {
                                    echo $return->get_error_message();
                                }
                                else {
                                    ReecesRainbowGrants::custom_log("Error setting ages 6-9 special grant");
                                    ReecesRainbowGrants::custom_log($return->get_error_message());
                                }
                            }                     
                        }
                    } 
                }

                if ( $age >= 10 ) {
                    $ds_10_plus_count++;
                    // $output .= get_the_title() . ' - age ' . $age ;
                    // $output .= ' post_id: ' . $post_id;

                    $terms = get_the_terms( $post->ID, 'special-grants' );
                    if ( ! empty( $terms ) ) {
                        foreach ( $terms as $term ) {
                            if ($term->slug == 'ages-6-9-ds') {
                                // echo " removing old grant";
                                $return = wp_remove_object_terms( $post_id, $term->term_id, 'special-grants' );
                                if( is_wp_error( $return ) ) {
                                    if ($manual) {
                                        echo $return->get_error_message();
                                    }
                                    else {
                                        ReecesRainbowGrants::custom_log("Error removing ages 6-9 special grant");
                                        ReecesRainbowGrants::custom_log($return->get_error_message());
                                    }
                                } 
                                $ds_6_to_9_removed_from_10_plus++;
                            }
                        }
                    }
        
                    $args = array(
                        'taxonomy'      => 'special-grants', 
                        'hide_empty'    => false,
                        'fields'        => 'all',
                        'slug'    => 'ages-10-ds'
                    );                             
                    $terms = get_terms( $args );                            
                    $count = count($terms);
                    if($count > 0){
                        foreach ($terms as $term) {
                            // echo ' term_id: ' . $term->term_id;
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'special-grants', true );   
                            if( is_wp_error( $return ) ) {
                                if ($manual) {
                                    echo $return->get_error_message();
                                }
                                else {
                                    ReecesRainbowGrants::custom_log("Error setting ages 10+ special grants");
                                    ReecesRainbowGrants::custom_log($return->get_error_message());
                                }
                            }                      
                        }
                    } 
                    // $output .= "<br>";
                }
                // echo '<h2 class="post-title entry-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
            } // end loop thru DS child grants
            $output .= $ds_6_to_9_count . " child grants set to DS 6-9 special grant<br>";
            $output .= $ds_10_plus_count . " child grants set to DS 10+ special grant<br>";
            $output .= $ds_6_to_9_removed_from_10_plus . " child grants for DS 10+ adjusted to no longer have DS 6-9 special grant";
            if ($manual) {
                echo $output . "<br>";
            }
            else {
                ReecesRainbowGrants::custom_log($output);
            }
            // Restore original Post Data
            wp_reset_postdata();
        }
    }

    function get_goal_total($cause_code) {
        // $this->db->show_errors();
        $total = 0;
        // echo "total: " . $total . "<br>";
        $donations_table = $this->db->prefix . "donation_can_paypal_donations";
        // echo "table: " . $donations_table . "<br>";
        $query = "SELECT SUM( amount ) FROM $donations_table WHERE cause_code =  '$cause_code' AND deleted = 0;";
        // echo "query: " . $query . "<br>";
        $total = $this->db->get_var($query);
        return $total;
    }

    function update_sizeablegrants($manual) {
        // get lowest minimum for sizeable grants
        $args = array(
            'taxonomy'      => 'sizeable-grants', 
            'hide_empty'    => false,
            'meta_key'      => 'minimum',
            'orderby'       => 'meta_value_num',
            'fields'        => 'all',
            'number'        => 1,
        ); 
        $terms = get_terms( $args );   
        foreach ( $terms as $term ) { 
            // var_dump($term);
            $lowest_minimum = get_field('minimum',$term->taxonomy . '_' . $term->term_id);
            // var_dump($lowest_minimum);
        }
        // var_dump($lowest_minimum);

        $args = array(
            'post_type' => 'childgrant',
            'orderby' => 'modified',
            'order'   => 'DESC',
            'nopaging' => true,
        );
        // echo "<br>";
        $childgrant_query = new WP_Query($args);
        // var_dump($childgrant_query);
        $count = 0;
        $sizable_grant_count = 0;
        
        if ( $childgrant_query->have_posts() ) {
            // echo "made it to IF<br>";
            while ( $childgrant_query->have_posts() ) {
                $count++;
                $childgrant_query->the_post();
                // echo "In loop, checking post # " . $count . " for " . get_the_title() . "<br>";
                $post_id = get_the_ID();
                $cause_code = get_field('cause_code');
                if ( !empty($cause_code) ) {
                    // echo "cause_code: " . $cause_code . "<br>";
                    $total = $this->get_goal_total($cause_code);
                    // echo "total " . $total . "<br>";

                    if  ( $total >= $lowest_minimum ) {
                        // do query for which sizeable grant this fits into ($total >= minimum AND $total <= maximum )
                        $args = array(
                            'taxonomy'      => 'sizeable-grants', 
                            'hide_empty'    => false,
                            'meta_key'      => 'minimum',
                            'orderby'       => 'meta_value_num',
                            'fields'        => 'all',
                            'number'        => 1,
                        ); 
                        $meta_query = array(
                            array(
                                'key' => 'minimum',
                                'value'     => $total,
                                'compare'   => '<=',
                            ),
                            array(
                                'key' => 'maximum',
                                'value'     => $total,
                                'compare'   => '>=',
                            ));
                        $args['meta_query'] = $meta_query;
                        $terms = get_terms( $args );   
                        foreach ( $terms as $term ) { 
                            $return = wp_set_post_terms( $post_id, $term->term_id, 'sizeable-grants', false );   
                            // echo "setting to " . $term->name;
                            if( is_wp_error( $return ) ) {
                                if ($manual) {
                                    echo $return->get_error_message();
                                }
                                else {
                                    ReecesRainbowGrants::custom_log("Error setting sizeable grant category");
                                    ReecesRainbowGrants::custom_log($return->get_error_message());
                                }
                            } 
                        }
                        $sizable_grant_count++;
                    } // end if total is in sizable grant range
                } // end if cause code is not empty
            } // end loop thru child grants with cause code set
            $output .= $sizable_grant_count . " posts set to sizeable grant categories";
            if ($manual) {
                echo $output . "<br>";
            }
            else {
                ReecesRainbowGrants::custom_log($output);
            }
            // Restore original Post Data
            wp_reset_postdata();
        }
    }

    function update_all_child_grants($manual) {
        $args = array(
            'post_type' => 'childgrant',
            'orderby' => 'modified',
            'order'   => 'DESC',
            'nopaging' => true,
        );
        $childgrant_query = new WP_Query($args);
        $count = 0;
        $newlylisted_count = 0;
        $custom_added_count = 0;
        $custom_removed_count = 0;
        $cause_code_count = 0;
        $at_risk_count = 0;
        $today = new DateTime('today', new DateTimeZone(wp_timezone_string()));
        // loop thru all child grant posts 
        if ( $childgrant_query->have_posts() ) {
            while ( $childgrant_query->have_posts() ) {
                $count++;
                $childgrant_query->the_post();
                $post_id = get_the_ID();

                // update newly_listed boolean field to be true if listed or published in past 90 days
                $listed_date = get_field('listed_date');
                // if the post has a listed date, use that -- otherwise use the post publish date
                if ( $listed_date ) {
                    $post_date = $listed_date;
                }
                else {
                    $post_date = get_the_date('');
                }
                $post_datetime = new DateTime( $post_date, new DateTimeZone( wp_timezone_string() ) );
                $post_age_diff = $post_datetime->diff($today);
                $post_age = $post_age_diff->days;
                // if (!$manual) {
                //     ReecesRainbowGrants::custom_log("Post " . $post_id . "; listed date:" . $post_datetime->format("Y-m") . "; age: " . $post_age);
                // }
                if ( $post_age <= 90 ) {
                    update_field( 'newly_listed', 1 );
                    $newlylisted_count++;
                }
                else {
                    update_field( 'newly_listed', 0, );
                } // end if $post_age <= 90

                // check if grant cause is equal to zero or is below custom goal goal amount; if so, set custom_goal boolean to true
                $cause_code = get_field('cause_code');
                if ( $cause_code ) {
                    $custom_goal_amount = get_option('custom_goal_amount');
                    $cause_code_count++;
                    $total = $this->get_goal_total($cause_code);
                    if ( $total == 0 || $total < $custom_goal_amount) {
                        // if (!$manual) {
                        //     ReecesRainbowGrants::custom_log(get_the_title() . " - Post " . $post_id . "; cause code:" . $cause_code . "; total: " . $total);
                        // }
                        update_field( 'custom_goal', 1 );
                        $custom_added_count++;
                    }
                    else {
                        update_field( 'custom_goal', 0, );
                        $custom_removed_count++;
                    } 
                } // end if $cause_code exists

                // check for at-risk age (older than 14.5 years old ) and set at_risk_of_aging_out boolean field, if they are not already marked Aged Out
                $aged_out = get_field('aged_out');
                $birth_date = new DateTime((get_field('birth_date')));
                $date_diff = $birth_date->diff($today);
                $age_years = $date_diff->y;
                $age_months = $date_diff->m;
                
                if ( ( ($age_years == 14 && $age_months >= 6) || $age_years >= 15) && !$aged_out )  {
                    $at_risk_count++;
                    update_field( 'at_risk_of_aging_out', 1 );
                }
                
            } // end loop thru child grants 
            $custom_goal_name = get_option('custom_goal_name');
            $output .= "Checked " . $count . " grants and set " . $newlylisted_count . " posts to newly listed and " . $at_risk_count . " grants to at risk of aging out<br>";
            $output .= "On " . $cause_code_count . " grants with cause codes, set " . $custom_added_count . " grants to " . $custom_goal_name . ", un-set " . $custom_goal_name . " on " . $custom_removed_count . " grants";
            if ($manual) {
                echo $output . "<br>";
            }
            else {
                ReecesRainbowGrants::custom_log($output);
            }
            // Restore original Post Data
            wp_reset_postdata();
        }
    }

    function setup_cron() {
        add_filter('reecesrainbow/daily_updates', [$this, 'daily_updates']);

        if (!wp_next_scheduled('reecesrainbow/daily_updates')) {
            wp_schedule_event(time(), 'daily', 'reecesrainbow/daily_updates');
        }        
    }

    function deactivation_tasks () {
        $timestamp = wp_next_scheduled( 'reecesrainbow/daily_updates' );
        wp_unschedule_event( $timestamp, 'reecesrainbow/daily_updates' );
    }
    
}