<?php

class Reports {

    public function __construct(){
        $this->db = $GLOBALS['wpdb'];
    
        /***** REGISTER CUSTOM POST TYPE *****/
        add_action('init', [$this, 'register_reports_cpt']);

        /***** Add options to menu *****/
        add_action( 'admin_menu', [$this, 'register_report_options_page']);

        /* load custom title  */
        add_filter( 'the_title', [$this, 'filter_report_title'] );

        /***** ADD A SHORTCODE *****/
        add_shortcode('rr-reports', [$this, 'display_reports']);
    }
    
    function register_report_options_page() {
        add_submenu_page(
            'edit.php?post_type=rr-report',
            __( 'Options', 'text_domain' ),
            __( 'Options', 'text_domain' ),
            'publish_posts',
            'rr-report-options',
            array($this, 'report_options_page_content')
        );
    }   

    /* Display content for the reports options page */
    function report_options_page_content() { 
        if (isset($_POST['step'])) {
            $step = intval($_POST["step"]);
            }
        else {
            $step = 0;
        }
        $error = false;
        switch ($step) {
            case 0:
                break;
            case 1:
                $error = update_option( 'report_intro', $_POST['report_intro'] );
                break;
        }
        echo '<p><div><h3>Report Introduction Text:</h3></div><div class="wrap"> <form action="" method="POST">
			<input type="hidden" name="step" value="1" />
			<textarea name="report_intro" rows="5" cols="80">';
        echo get_option( 'report_intro' ); 
        echo '</textarea>
            <input type="submit" name="submit" class="page-title-action" value="Update" /> ';
            
        
        echo '</input>
			</form> </div>';
    }

    // Register Custom Post Type
    function register_reports_cpt() {

        $labels = array(
            'name'                  => _x( 'Reports', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Report', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Reports', 'text_domain' ),
            'name_admin_bar'        => __( 'Report', 'text_domain' ),
            'archives'              => __( 'Report Archives', 'text_domain' ),
            'attributes'            => __( 'Report Attributes', 'text_domain' ),
            'parent_item_colon'     => __( 'Parent Post', 'text_domain' ),
            'all_items'             => __( 'All Reports', 'text_domain' ),
            'add_new_item'          => __( 'Add New Report', 'text_domain' ),
            'add_new'               => __( 'Add New', 'text_domain' ),
            'new_item'              => __( 'New Report', 'text_domain' ),
            'edit_item'             => __( 'Edit Report', 'text_domain' ),
            'update_item'           => __( 'Update Report', 'text_domain' ),
            'view_item'             => __( 'View Report', 'text_domain' ),
            'view_items'            => __( 'View Reports', 'text_domain' ),
            'search_items'          => __( 'Search Reports', 'text_domain' ),
            'not_found'             => __( 'No Reports found', 'text_domain' ),
            'not_found_in_trash'    => __( 'No Reports found in Trash', 'text_domain' ),
            'featured_image'        => __( 'Featured Image', 'text_domain' ),
            'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
            'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
            'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
            'insert_into_item'      => __( 'Insert into Report', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Report', 'text_domain' ),
            'items_list'            => __( 'Reports list', 'text_domain' ),
            'items_list_navigation' => __( 'Reports list navigation', 'text_domain' ),
            'filter_items_list'     => __( 'Filter Reports list', 'text_domain' ),
        );

        $args = array(
            'label'                 => __( 'Report', 'text_domain' ),
            'description'           => __( 'Reeces Rainbow Reports', 'text_domain' ),
            'labels'                => $labels,
            'rewrite'               => array('slug' => 'report'),
            'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
            'taxonomies'            => array( '' ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-media-document',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
        );
        register_post_type( 'rr-report', $args );

    }
    
    // enqueue custom CSS 
    function enqueue(): void {
        wp_enqueue_style( 'rr-reportcss', plugins_url('reecesrainbow') . '/css/report.css' , array(), '1.6.1' );
    }


    public function filter_report_title( $title ) {
        global $post;

        // Check if we're inside the main loop in a single post page.
        if (in_the_loop() && is_main_query() && $post->post_type == 'rr-report' ) {
            $this->enqueue();
        }
        return $title;
    }

    public function display_reports($attr) {
        global $paged;
        $posts_per_page = 50;
        $args = array(
            'post_type' => 'rr-report',
            'orderby' => 'publish_date',
            'order'   => 'DESC',
            'showposts' => $posts_per_page,
            'paged' => $paged
        );
        
        // var_dump($meta_query);
        $args['meta_query'] = $meta_query;
        $report_query = new WP_Query($args);
        $total_found_posts = $report_query->found_posts;
        $total_page = ceil($total_found_posts / $posts_per_page);
        //var_dump($report_query);
        if ( $report_query->have_posts() ) {
            ob_start();
            echo '<div class="report-intro">' . get_option( 'report_intro' ) . '</div>';
            echo '<div class="report-listpage">';
            global $report_listing;
            $report_listing = true;
            while ( $report_query->have_posts() ) {
                $report_query->the_post();
                echo '<article class="report post-entry">'; 
                echo '<h2 class="post-title entry-title"><a href="' . get_permalink() . '">';
                the_post_thumbnail( 'medium' ); 
                echo '<br>' . get_the_title() . '</a></h2></article>';
                echo '<span class="mobile-line"><hr></span>';
            }
            echo '</div>'; // end report-listpage div
            echo '<div class="report-nav">';
            $big = 999999999; // need an unlikely integer
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $report_query->max_num_pages
            ) );
            echo '</div>'; // end report-nav div
            $report_listing = false;
            $output = ob_get_clean();
            return $output;
            // Restore original Post Data
            wp_reset_postdata();
        }
        else {
            _e('Sorry, no matching reports were found.'); 
        }
    }

}