
var gulp = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
  
gulp.task('sass', function () {
  return gulp.src('./css/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest('./css'));
});

gulp.task('default', function () {
  gulp.watch('./css/**/*.scss', gulp.series(['sass']));
});

